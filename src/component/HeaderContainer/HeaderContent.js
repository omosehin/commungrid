import React, { Component } from 'react'
import '../../header/header.css';
import CustomeButton from '../customeButton';
import {Form,Button} from 'reactstrap';
export class HeaderContent extends Component {
    render() {
        return (
            <div className="container  ">
                <Form inline className=" py-5 pl-3">
                    <input type="text" className="searchField mt-2" placeholder="Find Affordable energe | tools">
                        {/* <FontAwesomeIcon icon={faSearch}/> */}
                    </input>

                    <Button className="searchButton">Search</Button>
                </Form>

                <h1 className="text-white font-weight-bold">Get connected to <br /> affordable energy <br /> systems</h1>
                <p className=" text-white">We provide unlimited access to a reliable range of <br /> tested and trusted energy providers and <br /> merchants</p>
                <Button outline ="outline" className="btn btn-outline-light btn-lg px-5 py-3 font-weight-bold">Start Now  </Button>
                
            </div>
        )
    }
}

export default HeaderContent
